#!/usr/bin/env groovy
package com.rbone.wikipedia_pathing

import org.xml.sax.SAXParseException

/**
 * * @author rbone.
 */
public class LinkExtractor {

    private static final def LINKED_FILES_FILE = new File("linked-files.txt")
    private static final long RUNTIME = 1000 * 60 * 0.25
    private static int depth = 0;

    private def route
    private def searchedLinks
    private def initialLink
    private def shortestRoute;

    private LinkExtractor() {
        this.route = new Stack<>()
        this.searchedLinks = new HashSet<>()
    }

    private boolean findHitler(String initialLink) {
        this.initialLink = initialLink
        return checkLinks(initialLink)
    }

    private boolean checkLinks(String pageLink) {
        route.push(pageLink);
        if (depth != 0 && route.size() >= depth) {
            return false
        }
        println("Checking page: " + pageLink)
        final def input = new URL(pageLink).getText()
        final def slurper = new XmlSlurper()
        /* Allow the doc type declaration as it's HTML */
        slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
        def doc
        try {
            doc = slurper.parseText(input)
        } catch (SAXParseException e) {
            println "Error parsing page, badly formed HTML: " + pageLink
            route.pop()
            searchedLinks.add(pageLink)
            return false
        }
        final List list = new ArrayList()
        /* Find the body content div, and then collect all links from it. */
        boolean foundHitler = false
        def linkedPage = null
        doc.depthFirst().collect { it }.find { it.name() == 'div' && it.@id == 'bodyContent' }.depthFirst().collect {
            it
        }.findAll { it.name() == "a" }.find {
            final def link = it.@href.text()
            if (link.startsWith("/wiki/Adolf_Hitler")) {
                println "Found Hitler"
                printRoute(route)
                /* Store links to pages that directly link to Hitler for improvements in subsequent runs. */
                saveLinkedPage(route.pop())
                foundHitler = true
                if(this.shortestRoute == null || this.route.size() < this.shortestRoute.size()){
                    shortestRoute = route
                }
                /* These returns are from the closures, which is why we store the result in predefined variables. */
                return true
            }
            if (isValidLink(link)) {
                list.add(link);
                final def url = new URL(pageLink)
                final def newLink = url.getProtocol() + "://" + url.getHost() + link;
                if (isLinkedPage(newLink)) {
                    linkedPage = newLink
                }
            }
            return false
        }
        if (foundHitler) {
            return true
        }
        if (linkedPage != null) {
            println "Found linked page - taking shortcut: " + linkedPage
            if (checkLinks(linkedPage)) {
                return true
            } else if(depth == 0) {
                println "Hitler was not found this time..."
                deleteLinkedPage(linkedPage)
            }
        }
        while (!list.isEmpty()) {
            int nextIndex = nextRandomNumber(list)
            final def link = list.get(nextIndex);
            final def url = new URL(pageLink)
            final def newLink = url.getProtocol() + "://" + url.getHost() + link;
            /* Check the link hasn't already been checked or isn't already in the route (which would end up in a loop). */
            if (route.contains(newLink) || searchedLinks.contains(newLink)) {
                list.remove(link);
                continue
            }
            if (checkLinks(newLink)) {
                return true
            }
            list.remove(link);
        }
        route.pop();
        searchedLinks.add(pageLink)
        return false
    }

    private void printShortestRoute() {
        println "Shortest route found: "
        if (this.shortestRoute == null) {
            this.shortestRoute = this.route
        }
        printRoute(this.shortestRoute)
    }

    private void resetCurrentRoute() {
        this.route = new Stack<>()
        this.searchedLinks = new HashSet<>()
    }

    private void printRoute(def route) {
        depth = route.size()
        println depth + " hops"
        println "Route: \n" + String.valueOf(route).replaceAll("[\\[\\]]", "").replaceAll(", ", " -> ").replaceAll("http://en.wikipedia.org/wiki/Special:Random", this.initialLink) + " -> http://en.wikipedia.org/wiki/Adolf_Hitler"
    }

    private static double nextRandomNumber(List list) {
        Math.random() * (list.size() - 1)
    }

    private static void saveLinkedPage(final def pageLinkedToHitler) {
        if (!isLinkedPage(pageLinkedToHitler)) {
            LINKED_FILES_FILE << (pageLinkedToHitler + "\n")
        }
    }

    private static void deleteLinkedPage(final def link) {
        def text = ''
        LINKED_FILES_FILE.eachLine { line ->
            if (!(line == link))
                text += line + "\n"
        }
        LINKED_FILES_FILE < text
    }

    private static def boolean isLinkedPage(final def link) {
        boolean isLinkedPage = false
        LINKED_FILES_FILE.find { line ->
            if (line == link) {
                isLinkedPage = true
                return true
            }
            return false
        }
        return isLinkedPage
    }

    private static boolean isValidLink(String link) {
        /* Only take wiki links that aren't pointing to files. */
        return link.startsWith("/wiki/") && !link.contains(':') && !link.contains("(disambiguation)")
    }

    /* Find out what the random article is. */

    private static def getFinalInitialPage(final def initialLink) {
        final def input = new URL(initialLink).getText()
        final def slurper = new XmlSlurper()
        /* Allow the doc type declaration as it's HTML */
        slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
        def doc
        try {
            doc = slurper.parseText(input)
        } catch (SAXParseException e) {
            println "Error parsing page, badly formed HTML: " + initialLink
            return initialLink;
        }
        def finalInitialLink = initialLink;
        doc.depthFirst().collect { it }.findAll { it.name() == 'link' && it.@rel == 'canonical' }.each {
            finalInitialLink = it.@href.text()
        }
        return finalInitialLink
    }

    public static void main(String[] args) {
        if (args.length != 2 && args.length != 1) {
            println "Usage: <link> -f|-s (optional)"
            println "f finds the first route to Hitler it comes across. This is the default if just a link is specified."
            println "s finds the shortest route to Hitler"
            System.exit(1)
        }
        if (!LINKED_FILES_FILE.exists()) {
            LINKED_FILES_FILE.createNewFile();
        }
        final def linkExtractor = new LinkExtractor();
        final def link = args[0]
        if (args.length == 2 && args[1] != '-s' && args[1] != '-f') {
            println "That isn't -f or -s, dumbass."
            System.exit(2)
        }

        final def finalInitialPage = getFinalInitialPage(link)
        if (args.length == 2 && args[1] == '-s') {
            if (linkExtractor.findHitler(finalInitialPage)) {
                long startTime = System.currentTimeMillis()
                while (System.currentTimeMillis() - startTime < RUNTIME) {
                    if(depth == 1){
                        System.exit(0)
                    }
                    linkExtractor.resetCurrentRoute()
                    if (linkExtractor.findHitler(finalInitialPage)) {
                        println "Found shorter route!!"
                    } else {
                        println "Route was not shorter..."
                    }
                }
                linkExtractor.printShortestRoute()
                System.exit(0)
            }
            System.exit(3)
        } else {
            if (linkExtractor.findHitler(finalInitialPage)) {
                System.exit(0)
            }
            System.exit(3)
        }
    }

}

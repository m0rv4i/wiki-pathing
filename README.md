Finds the shortest path to the Hitler Wikipedia page from a wikipedia page as per the Hitler game (find the quickest route to Hitler from a random page, without using disambiguations).

It will store previous routes in a file, so the more it is used the quicker it gets.

Usage:

    gradlew clean build
    build/libs/wiki-pathing-1.0.jar <link> (-f | -s)


-f: show the first route (default).

-s: find the shorted route.